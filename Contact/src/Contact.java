public class Contact {
  private String firstName;
  private String lastName;
  
  public Contact() {
	  this.firstName = "no first name";
	  this.lastName = "no last name";
  }
  
  public Contact(String firstName) {
	  this.firstName = firstName;
	  this.lastName = "no last name";	  
  }
  
  public Contact (String firstName, String lastName) {
    this.firstName = firstName;
    this.lastName = lastName;
  }
  
  public String getFirstName() {return firstName;}
  public String getLastName() {return lastName;}

  public void setFirstName(String firstName) {
	    this.firstName = firstName;	  
  }
  public void setLastName(String lastName) {
	    this.lastName = lastName;	  
  }
  
  public String getContact() {
    return firstName + " " + lastName;
  }

  @Override
  public String toString() {
    return "["+getContact()+"]";
  }
}